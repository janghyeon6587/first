package first.common.controller;

import java.io.File;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import first.common.common.CommandMap;
import first.common.model.JhResult;
import first.common.service.CommonService;
import first.common.util.RequestUtil;
import first.common.util.ResponseUtil;
import first.common.util.StringUtil;

@CrossOrigin(origins = "*", maxAge = 3600) 
@RestController //RestController로 정의되어 있어야 jsp파일을 읽을 수 있다. (@Controller + @ResponseBody)
//@Controller
public class CommonController {
	//Logger log = Logger.getLogger(this.getClass());
	
	
	//서비스단과 열결하기 위한 객체선언 및 @Autowired설정
	private final first.common.service.testService testService ;
	
	@Autowired
	CommonController(first.common.service.testService testService) {
		this.testService = testService;
	}
	
	@Resource(name="commonService")
	private CommonService commonService;
	
	@RequestMapping(value="/common/downloadFile.do")
	public void downloadFile(CommandMap commandMap, HttpServletResponse response) throws Exception{
		Map<String,Object> map = commonService.selectFileInfo(commandMap.getMap());
		String storedFileName = (String)map.get("STORED_FILE_NAME");
		String originalFileName = (String)map.get("ORIGINAL_FILE_NAME");
		
		byte fileByte[] = FileUtils.readFileToByteArray(new File("C:\\dev\\file\\"+storedFileName));
		
		response.setContentType("application/octet-stream");
		response.setContentLength(fileByte.length);
		response.setHeader("Content-Disposition", "attachment; fileName=\"" + URLEncoder.encode(originalFileName,"UTF-8")+"\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.getOutputStream().write(fileByte);
		
		response.getOutputStream().flush();
		response.getOutputStream().close();
	}

	//@ResponseBody
	@RequestMapping(value = "test")
	public String test(Map<String, Object> commandMap) throws Exception {
		return "hi";
	}
	
	@GetMapping(value = "infomation2")
	public ResponseEntity<?> infomation2(HttpServletRequest request) {
		HashMap<String, Object> param = RequestUtil.paramToHashMap(request);
		HashMap<String, Object> newParam = new HashMap<String, Object>();
		JhResult result = new JhResult();

		newParam.put("id", StringUtil.fixNull(param.get("id")));
		newParam.put("pw", StringUtil.fixNull(param.get("pw")));

		result = testService.testService(newParam);
		return ResponseUtil.JhResponse(result);
	}
}
