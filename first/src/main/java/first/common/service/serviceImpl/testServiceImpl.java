package first.common.service.serviceImpl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import first.common.model.JhResult;
import first.common.util.StringUtil;

@Service("testService")
public class testServiceImpl implements first.common.service.testService{
	
	private final first.common.mapper.testMapper testMapper;
	
	@Autowired
	testServiceImpl(first.common.mapper.testMapper testMapper){
		this.testMapper = testMapper;
	}
	
	
	public JhResult testService (HashMap<String, Object> param){
		HashMap<String, Object> newParam = new HashMap<String, Object>();
		JhResult result = new JhResult();

		newParam.put("id", StringUtil.fixNull(param.get("id")));
		newParam.put("pw", StringUtil.fixNull(param.get("pw")));
		
		List<HashMap<String, Object>> selectValue = testMapper.select(newParam);
		result.setResultData(selectValue);
		
		return result;
	}

}
